#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char **argv) {
    int stairs = 0;
    int step = 0;
    int total_steps = 0;

    if (argc == 3) {
        stairs = atoi(argv[1]);
        step = atoi(argv[2]);
        total_steps = stairs / (step + 1);
        if (stairs % (step + 1))
            total_steps++;
        printf("%d\n", total_steps);
    } else {
        write(2, argv[0], strlen(argv[0]));
        write(2, ": wrong number of arguments, expected 2\n", 40);
        exit(1);
    }
    return 0;
}
